require "application_system_test_case"

class EmpruntsTest < ApplicationSystemTestCase
  setup do
    @emprunt = emprunts(:one)
  end

  test "visiting the index" do
    visit emprunts_url
    assert_selector "h1", text: "Emprunts"
  end

  test "creating a Emprunt" do
    visit emprunts_url
    click_on "New Emprunt"

    fill_in "Idlivre", with: @emprunt.idlivre
    fill_in "Nombre", with: @emprunt.nombre
    click_on "Create Emprunt"

    assert_text "Emprunt was successfully created"
    click_on "Back"
  end

  test "updating a Emprunt" do
    visit emprunts_url
    click_on "Edit", match: :first

    fill_in "Idlivre", with: @emprunt.idlivre
    fill_in "Nombre", with: @emprunt.nombre
    click_on "Update Emprunt"

    assert_text "Emprunt was successfully updated"
    click_on "Back"
  end

  test "destroying a Emprunt" do
    visit emprunts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Emprunt was successfully destroyed"
  end
end
