class CreateEmprunts < ActiveRecord::Migration[5.2]
  def change
    create_table :emprunts do |t|
      t.integer :idlivre
      t.integer :nombre

      t.timestamps
    end
  end
end
