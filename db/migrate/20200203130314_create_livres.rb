class CreateLivres < ActiveRecord::Migration[5.2]
  def change
    create_table :livres do |t|
      t.string :titre
      t.string :auteur
      t.text :synopsis
      t.string :style
      t.integer :isbn

      t.timestamps
    end
  end
end
