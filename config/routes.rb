Rails.application.routes.draw do
  resources :emprunts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :livres
  post '/livres/tabledit' => 'livres#tabledit'
end
