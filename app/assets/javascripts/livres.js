$('#livres').Tabledit({
	url: '/livres/tabledit',
	editButton: true,
	deleteButton: true,
	buttons: {
		edit: {
			class: 'btn btn-dark',
			html: '<span class="fa fa-edit"></span> Edit',
			action: '/livres/tabledit'
		},
		delete: {
			class: 'btn btn-dark',
			html: '<span class="fa fa-trash"></span> Delete',
			action: 'delete'
		},
		save: {
			class: 'btn btn-success btn-sm',
			html: '<span class="fa fa-save"></span> Save',
			action: '/livres/update'
		},
		restore: {
			class: 'btn btn-warning',
			html: '<span class="fa fa-trash-restore"></span> Restore',
			action: 'restore'
		},
		confirm: {
			class: 'btn btn-danger btn-sm',
			html: '<span class="fa fa-check"></span> Confirm'
		}
	},
	hideIdentifier: true,
	columns: {
		identifier: [0, 'id'],
		editable: [[1, 'titre'], [2, 'auteur'], [3, 'synopsis'], [4, 'style'], [5, 'isbn']]
	},
	onSuccess: function(data, textStatus, jqXHR) {
		console.log('onSuccess(data, textStatus, jqXHR)');
		console.log(data);
		console.log(textStatus);
		console.log(jqXHR);
	},
	onAjax: function(action, serialize) {
		console.log('onAjax(action, serialize)');
		console.log(action);
		console.log(serialize);
	}
});
