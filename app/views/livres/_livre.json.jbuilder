json.extract! livre, :id, :titre, :auteur, :synopsis, :style, :isbn
json.url livre_url(livre, format: :json)
