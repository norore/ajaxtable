json.extract! emprunt, :id, :idlivre, :nombre, :created_at, :updated_at
json.url emprunt_url(emprunt, format: :json)
