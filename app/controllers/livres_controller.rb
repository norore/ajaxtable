class LivresController < ApplicationController
  before_action :set_livre, only: [:show, :edit, :update, :destroy]

  def index
    @livres = Livre.order(id: :desc)
  end

  def new
    @livre = Livre.new
  end

  def edit
  end

  def create
    @livre = Livre.new(livre_params)
    respond_to do |format|
      if @livre.save(livre_params)
        format.js
        format.html { redirect_to livres_path, notice: 'livre was successfully added.' }
        format.json { render :index, status: :ok, location: livres_path }
      else
        format.html { render :new }
        format.json { render json: @livre.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @livre.update(livre_params)
          format.js
          format.html { redirect_to @livre, success: 'livre mis à jour' }
          format.json { render :index, status: :ok, location: livres_path }
      else
        format.html { render edit_livre_path(@livre) }
        format.json { render json: @livre.errors, status: :unprocessable_entity }
      end
    end
  end

  # interaction avec table-edit
=begin
  def tabledit
    if params[:edit]
      @livre = Livre.find(params[:id])
      respond_to do |format|
        if @livre.update(livre_params)
          format.html { redirect_to @livre, notice: 'livre was successfully updated.' }
          format.json { render :index, status: :ok, location: livres_path }
        else
          format.html { render :edit }
          format.json { render json: @livre.errors, status: :unprocessable_entity }
        end
      end
    end
  end
=end

  def destroy
    @livre.destroy
    respond_to do |format|
      format.js
      format.html { redirect_to livres_url, success: 'livre supprimé' }
      format.json { head :no_content }
    end
    #redirect_to livres_url, success: 'livre supprimé' 
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_livre
    @livre = Livre.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def livre_params
    params.require(:livre).permit(
      :titre,
      :auteur,
      :synopsis,
      :style,
      :isbn
    )
  end
end
