class EmpruntsController < ApplicationController
  before_action :set_emprunt, only: [:show, :edit, :update, :destroy]

  # GET /emprunts
  # GET /emprunts.json
  def index
    @emprunts = Emprunt.all
  end

  # GET /emprunts/1
  # GET /emprunts/1.json
  def show
  end

  # GET /emprunts/new
  def new
    @emprunt = Emprunt.new
  end

  # GET /emprunts/1/edit
  def edit
  end

  # POST /emprunts
  # POST /emprunts.json
  def create
    @emprunt = Emprunt.new(emprunt_params)

    respond_to do |format|
      if @emprunt.save
        format.html { redirect_to @emprunt, notice: 'Emprunt was successfully created.' }
        format.json { render :show, status: :created, location: @emprunt }
      else
        format.html { render :new }
        format.json { render json: @emprunt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emprunts/1
  # PATCH/PUT /emprunts/1.json
  def update
    respond_to do |format|
      if @emprunt.update(emprunt_params)
        format.html { redirect_to @emprunt, notice: 'Emprunt was successfully updated.' }
        format.json { render :show, status: :ok, location: @emprunt }
      else
        format.html { render :edit }
        format.json { render json: @emprunt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emprunts/1
  # DELETE /emprunts/1.json
  def destroy
    @emprunt.destroy
    respond_to do |format|
      format.html { redirect_to emprunts_url, notice: 'Emprunt was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emprunt
      @emprunt = Emprunt.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def emprunt_params
      params.require(:emprunt).permit(:idlivre, :nombre)
    end
end
