= Créer un tableau éditable en Ruby-On-Rails



== Initialisation du projet

Création du projet :

[source, sh]
----
rails new ajaxtable
----

Préparation de la migration par création de la table Livres :

[source, sh]
----
bin/rails g model livres titre:string auteur:string synopsis:text style:string isbn:integer
----

Préparation des gems :

[source, ruby]
----
# /Gemfile
source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'rails', '~> 5.2.4', '>= 5.2.4.1'
gem 'puma', '~> 3.11'

gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'

gem 'bootsnap', '>= 1.1.0', require: false

gem 'bootstrap', '~> 4.3.1'
gem 'jquery-rails'
gem 'jquery-ui-rails'

group :development, :test do
  gem 'sqlite3'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'guard-rails', require: false
  gem 'guard-livereload', require: false
  gem 'rack-livereload'
  gem 'rerun'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
----

Installation des gems :

[source, bash]
----
bundle install
----

Initialisation de la base de données :

[source, ruby]
----
# /db/seeds.rb
Livre.create(
  titre: 'Une porte sur l’été',
  auteur: 'Robert A. Heinlein',
  synopsis: 'Daniel B. Davis s’aperçoit que sa fiancée et son meilleur ami l’ont trahi et évincé de son usine de fabrication de robots. Il décide de fuir vers l’avenir, en compagnie de son chat, Petronius le Sage, même si cela doit l’obliger à quitter Ricky, une petite fille qu’il aime tendrement. Daniel choisit la route du « long sommeil », c’est-à-dire celle de l’hibernation artificielle. Mais son chat disparaît et c’est seul qu’il affronte le bond dans le temps. Par quel miracle, trente ans plus tard, Rocky est-elle âgée de vingt ans à peine et Petronius le Sage, présent dans ce monde du futur ?',
  style: 'Science-Fiction',
  isbn: 2277125105
)
Livre.create(
  titre: 'Dune',
  auteur: 'Frank Herbert',
  synopsis: 'Il n’y a pas, dans tout l’Empire, de planète plus inhospitalière que Dune. Partout des sables à perte de vue. Une seule richesse : l’épice de longue vie, née du désert, et que tout l’univers achète à n’importe quel prix. Richesse très convoitée : quand Leto Atréides reçoit Dune en fief, il flaire le piège. Il aura besoin des guerriers Fremen qui, réfugiés au fond du désert, se sont adaptés à une vie très dure en préservant leur liberté, leurs coutumes et leur foi mystique. Ils rêvent du prophète qui proclamera la guerre sainte et qui, à la tête des commandos de la mort, changera le cours de l’histoire. Cependant les Révérendes Mères du Bene Gesserit poursuivent leur programme millénaire de sélection génétique ; elles veulent créer un homme qui concrétisera tous les dons latents de l’espèce. Tout est fécond dans ce programme, y compris ses défaillances. Le Messie des Fremen est-il déjà né dans l’Empire ?',
  style: 'Science-Fiction',
  isbn: 2221026020
)
Livre.create(
  titre: 'Le bureau des atrocités',
  auteur: 'Charles Stross',
  synopsis: 'On vous a menti sur toute l’histoire contemporaine. Durant la Seconde Guerre mondiale, les nazis ont failli l’emporter grâce à leurs sacrifices humains et leurs évocations des puissances ténébreuses qui rôdent derrière la porte d’autres univers. L’informaticien Bob Howard (dont le nom semble inspiré du premier prénom de H. P. Lovecraft) a été engagé de force au Bureau des Atrocités, dit aussi la Laverie Centrale, parce qu’il a eu le malheur d’explorer des archives qui auraient dû être effacées. Et d’y apprendre la thaumaturgie mathématique. En effet, la Laverie, le plus secret des services secrets britanniques, veille à ce que certains théorèmes qui ouvrent l’accès sur d’autres univers ne soient jamais redécouverts. Elle enquête accessoirement sur tous les phénomènes étranges aux fins de les résorber. Ce qui n’exclut pas la bureaucratie la plus tatillonne. Howard est l’un de ses agents qualifiés action. Précisément, il lui faut aller aux Etats-Unis récupérer un chercheur auquel semblent s’intéresser des terroristes. Une chercheuse plutôt, rousse aussi flamboyante qu’intelligente. Et Howard se retrouve sur la piste de l’Ahnenerbe, le plus mystérieux des organismes nazis, qui aurait survécu un demi-siècle sur un autre monde, dans un autre univers. Grâce peut-être à l’aide d’entités à côté desquelles Cthulhu est un gentil mickey. Issu d’un croisement improbable entre James Hadley Chase, Ian Fleming et H.-P. Lovecraft, X-Files et Men in Black, ce roman déplace les frontières entre genres. Et Charles Stross s’y montre désopilant autant que terrifiant.',
  style: 'Science-Fiction',
  isbn: 2253123684
)
Livre.create(
  titre: 'La mythologie du monde celte',
  auteur: 'Claude Sterckx',
  synopsis: 'Mystérieux Celtes ? Oui et non… Oui parce qu’on les croit généralement enveloppés d’une aura hors du commun… Non, parce que les chercheurs ont mis au jour bien des aspects de leur personnalité et de leur mode de vie… Oui encore, parce que ces travaux restent trop mal connus et que, trop souvent, des élucubrations alléchantes mais largement fantasmatiques tiennent le devant de la scène… Laissant les fantasmes à ceux qu’ils séduisent, la recherche de la vérité est un jeu gratifiant qui laisse voir que les traditions des anciens Celtes étaient étonnamment subtiles et que les formes dans lesquelles ils les ont le mieux exprimées - mythes, contes et légendes - surpassent souvent les meilleurs romans ! Le présent livre propose d’abord une introduction à l’état actuel des connaissances puis s’attache à une analyse des conceptions du monde telles que les révèle la mythologie : car celle-là n’est pas un simple recueil de contes mais une tentative, dans un langage imagé, d’expliquer le fonctionnement et le destin de l’univers en fonction des connaissances scientifiques et de la réflexion philosophique de l’époque.',
  style: 'Histoire',
  isbn: 2501054105
)
Livre.create(
  titre: 'Xenos',
  auteur: 'Dan Abnett',
  synopsis: '« La pensée engendre l’hérésie - l’hérésie appelle le châtiment ». Infiltrée au cœur de l’humanité, l’Inquisition la sillonne comme une ombre vengeresse, poursuivant et abattant impitoyablement ses ennemis avec une intransigeance absolue. L’inquisiteur Eisenhorn se trouve confronté à une conjuration interstellaire de grande envergure ainsi qu’à des sombres puissances démoniaques qui toutes rivalisent pour la possession d’un texte ésotérique d’une abominable puissance - un antique grimoire connu sous le nom de Nécrothèque. Découvrez le premier roman der la trilogie du célèbre Inquisiteur Eisenhorn et percez les mystères de l’un des meilleurs univers de science-fiction, celui de Warhammer 40 000 !',
  style: 'Fantasy',
  isbn: 2915989273
)
----

Migration de la base de données avec les données :

[source, bash]
----
bin/rake db:create db:migrate db:seed
----

Lancement de l’application :

[source, bash]
----
bin/rails s
----

Ça marche, on peut arrêter à tout moment le serveur avec la combinaison de touches contrôle+D.

== Tableau des livres

[source, bash]
----
bin/rails g controller Livres index
----

[source, ruby]
----
# /app/controllers/livres_controller.rb
class LivresController < ApplicationController
  def index
    @livres = Livre.all
  end
end
----

[source, ruby]
----
# /app/views/livres/index.html
<h1>Tableau de mes livres</h1>

<% if @livres.size > 0 %>

  <table>
    <thead>
      <tr>
        <th>Titre</th>
        <th>Auteur</th>
        <th>Synopsis</th>
        <th>Style</th>
        <th>ISBN</th>
      </tr>
    </thead>

    <tbody>
      <% @livres.each do |livre| %>
        <tr>
          <td><%= livre.titre %></td>
          <td><%= livre.auteur %></td>
          <td><%= livre.synopsis[0..120] %></td>
          <td><%= livre.style %></td>
          <td><%= livre.isbn %></td>
        </tr>
      <% end %>
    </tbody>
  </table>

<% end %>
----

== Sources d’inspiration

* https://stackoverflow.com/questions/23926204/updating-table-data-in-rails-using-ajax
* https://medium.com/@jelaniwoods/how-to-actually-use-ajax-in-rails-83e667ea7953
* https://dev.nozav.org/rails_ajax_table.html

== Sources d’idée pour les Livres

* https://www.outrelivres.fr/[Outre Livres], tenu par mon amie Stéphanie Chaptal
* Les synopsis et numéros ISBN viennent du site https://www.babelio.com[Babelio]
